package victor.logparser;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        String[] logs = new String[]{
                "171 1613587382 Add",
                "300 1613587388 Add",
                "300 1613587392 Browse",
                "8119 1613587433 Click",
                "300 1613587475 Click",
                "171 1613587481 Browse",
                "8119 1613587519 Click",
                "171 1613587555 Add",
                "8119 1613587700 Drop"};

        printTrees(buildTrees(logs), 0);
    }

    private static void printTrees(Map<ActionTree, ActionTree> trees, int level) {
        if (trees != null) {
            trees.forEach((key, tree) -> {
                String spaces = new String(new char[level]).replace('\0', ' ');
                System.out.printf("%s%s(%d)%n", spaces, tree.action, tree.count);
                printTrees(tree.getChildren(), level + 1);
            });
        }
    }

    private static Map<ActionTree, ActionTree> buildTrees(String[] logs) {
        List<ActionTree> trees = new LinkedList<>();
        Map<Integer, ActionTree> userIndex = new HashMap<>();

        Arrays.stream(logs)
                .map(LogLine::new)
                .forEach(logLine -> {
                    if (userIndex.containsKey(logLine.getUser())) {
                        ActionTree actionTree = userIndex.get(logLine.getUser());
                        while (!actionTree.children.isEmpty()) {
                            actionTree = actionTree.getChildren().keySet().iterator().next();
                        }
                        ActionTree child = new ActionTree(logLine.getAction(), 1);
                        actionTree.getChildren().put(child, child);
                    } else {
                        ActionTree actionTree = new ActionTree(logLine.getAction(), 1);
                        userIndex.put(logLine.getUser(), actionTree);
                        trees.add(actionTree);
                    }
                });

        return collapseTrees(trees, new HashMap<>());
    }

    private static Map<ActionTree, ActionTree> collapseTrees(List<ActionTree> trees, Map<ActionTree, ActionTree> collapsed) {
        trees.forEach(tree -> {
            if (collapsed.containsKey(tree)) {
                collapsed.get(tree).increment();
                collapsed.get(tree).setChildren(collapseTrees(new LinkedList<>(tree.getChildren().keySet()), collapsed.get(tree).getChildren()));
            } else {
                collapsed.put(tree, tree);
            }
        });

        return collapsed;
    }
}

class LogLine {
    private int user;
    private long timestamp;
    private String action;

    public LogLine(String log) {
        String[] parts = log.split("\\s+");
        user = Integer.parseInt(parts[0]);
        timestamp = Long.parseLong(parts[1]);
        action = parts[2];
    }

    public int getUser() {
        return user;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getAction() {
        return action;
    }
}

class ActionTree {
    String action;
    int count;
    Map<ActionTree, ActionTree> children;

    public ActionTree(String action, int count) {
        this.action = action;
        this.count = count;
        this.children = new HashMap<>();
    }

    public String getAction() {
        return action;
    }

    public int getCount() {
        return count;
    }

    public Map<ActionTree, ActionTree> getChildren() {
        return children;
    }

    public void setChildren(Map<ActionTree, ActionTree> children) {
        this.children = children;
    }

    public void increment() {
        this.count++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActionTree that = (ActionTree) o;
        return Objects.equals(action, that.action);
    }

    @Override
    public int hashCode() {
        return Objects.hash(action);
    }
}
